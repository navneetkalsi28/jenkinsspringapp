package com.training.jdbc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JenkinsspringappApplication {

	public static void main(String[] args) {
		SpringApplication.run(JenkinsspringappApplication.class, args);
	}
}
